const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = [
  {
    test: /moodboard.html$/,
    use: ['raw-loader'],
    exclude: /node_modules/
  },
  {
    test: /\.css$/, use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: 'css-loader'
    }),
    exclude: /node_modules/
  },
  {
    test: /\.js$/,
    exclude: /node_modules/,
    use: [{
      loader: 'babel-loader',
      query: {
        plugins: [
          'transform-object-rest-spread',
          'transform-es2015-arrow-functions',
          'transform-es2015-template-literals'
        ]
      }
    }],
  },
  {
    test: /\.scss$/, use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        {
          loader: 'css-loader',
          options: {
            minimize: true,
            url: false
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            plugins: function() { return []; },
            sourceMap: false
          },
        },
        'sass-loader'
      ]
    }),
    exclude: /node_modules/
  },
  { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: ['url-loader?limit=10000&minetype=application/font-woff'], exclude: /node_modules/ },
  { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: ['file-loader'], exclude: /node_modules/ },
  { test: /\.jpe?g$|\.gif$|\.png$|\.mp4$|\.webm$/i, use: ['url-loader?limit=0'], exclude: /node_modules/ }
];