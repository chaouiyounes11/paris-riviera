const StringReplacePlugin = require('string-replace-webpack-plugin');
const locale = require('../../locale/en_US.json');

module.exports = [
  {
    test: /moodboard.html$/,
    loader: StringReplacePlugin.replace({

      replacements: [{
        pattern: /\$\{[A-Za-z0-9_.-]+\}/g,
        replacement: function(match) {
          const pureString = match.replace('${', '').replace('}', '');
          const words = pureString.split('.');
          let jsStringToEval = words.map(w => '["'+w+'"]').join('');
          try {
            return eval('locale' + jsStringToEval);
          }
          catch (e) {
            return 'undefined';
          }
        }
      },
      {
        pattern: /undefined\/w_0\.51\,h_0\.51\,c_crop\/f_jpg\/undefined/g,
        replacement: function() {
          return 'https://prd-v3-i.chanel.com/fnbv3/image/full/chanel__com_type1/skus_full/les-beiges-teint-belle-mine-naturelle-spf-25--pa-n20-30ml.3145891842203.jpg';
        }
      }]
    })
  },
  { test: /\.css$/, use: ['style-loader', 'css-loader'], exclude: /node_modules/ },
  {
    test: /\.js$/,
    exclude: /node_modules/,
    use: [{
      loader: 'babel-loader',
      query: {
        plugins: [
          'transform-object-rest-spread',
          'transform-es2015-arrow-functions',
          'transform-es2015-template-literals'
        ]
      }
    }],
  },
  {
    test: /\.scss$/,
    use: [
      'style-loader',
      'css-loader',
      {
        loader: 'postcss-loader',
        options: {
          plugins: function() { return []; },
          sourceMap: true
        },
      },
      'sass-loader'
    ],
    exclude: /node_modules/
  },
  { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: ['url-loader?limit=10000&minetype=application/font-woff'], exclude: /node_modules/ },
  { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: ['file-loader'], exclude: /node_modules/ },
  { test: /\.jpe?g$|\.gif$|\.png$|\.tif$|\.mp4$|\.webm$/i, use: ['url-loader?limit=4096'], exclude: /node_modules/ }
];
