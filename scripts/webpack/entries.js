var path = require('path');

module.exports = {
  app: path.resolve('./javascripts/app.js'),
  css: path.resolve('./css/global.scss')
}
