const fs = require('fs');
const glob = require('glob');
const _ = require('lodash');
const uniqueString = require('unique-string');

if (!fs.existsSync('livraison/FNB')) {
  return;
}

// Build i18n files
glob('**/locale/*.json', {}, function (er, files) {
  files.forEach(function(file) {
    let langContent = require('../' + file);
    try {
      const langContentPlatformSpecific = require('../' + file.replace('locale/', 'locale/FNB/'));
      langContent = _.merge(langContent, langContentPlatformSpecific);
    }
    catch (e) {}
    const langCode = file.replace('locale/', '').replace('.json', '');
    let str = 'DATA:=' + JSON.stringify(langContent);
    fs.writeFile('livraison/FNB/' + langCode + '.properties', str, err => {});
  });
});

// Update CSS/JS version needed for AEM to work properly
fs.readFile('livraison/FNB/index.html', 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }

  data = data.replace(/\?v=(\w+)/g, '?v=' + uniqueString());
  fs.writeFile('livraison/FNB/index.html', data, 'utf8', function(err) {});
});