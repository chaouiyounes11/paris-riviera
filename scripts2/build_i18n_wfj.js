const fs = require('fs');
const glob = require('glob');
const _ = require('lodash');

const pathToWFJPath = (path) => {
  const newString = path
    .replace(/\//g, '_')
    .replace(/\s+/g, '_')
    .replace(/\.[^/.]+$/, '')
    .toLowerCase()
    .replace('.._images_', '$media_')
    .replace('._images_', '$media_');
  return newString;
};

if (!fs.existsSync('livraison/WFJ')) {
  return;
}

// Build i18n files
glob('**/locale/*.json', {}, function (er, files) {
  files.forEach(function(file) {
    let langContent = require('../' + file);
    try {
      const langContentPlatformSpecific = require('../' + file.replace('locale/', 'locale/WFJ/'));
      langContent = _.merge(langContent, langContentPlatformSpecific);
    }
    catch (e) {}
    const langCode = file.replace('locale/', '').replace('.json', '');
    let str = '';
    Object.keys(langContent).map(key => {
      str += key + ' = ' + langContent[key] + '\n';
    });
    fs.writeFile('livraison/WFJ/' + langCode + '.properties', str, err => {});
  });
});

fs.readFile('public/app.js', 'utf8', function (err, js) {
  const preJs = "PLATFORM = 'WFJ';";
  js = preJs + js;
  fs.writeFile('livraison/WFJ/app.js', js, 'utf8', function(err) {});
});

fs.readFile('moodboard.html', 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }

  // Search for entities in HTML
  const entities = [];
  const reg = /src=\"\.\/images\/([^"]+)/g;
  let result;
  while((result = reg.exec(data)) !== null) {
    entities.push('"../images/' + result[1]);
    const originPath = './images/' + result[1];
    const filePath = pathToWFJPath(originPath);
    data = data.replace(originPath, filePath);
  }

  data = data.replace(/\$\{/g, '$').replace(/\}/g, '');
  fs.readFile('public/app.css', 'utf8', function (err, css) {

    // Search for entities
    //const entities = [];
    const reg = /url\(\"\.\.\/images\/([^)]+)\)/g;
    let result;
    while((result = reg.exec(css)) !== null) {
      entities.push('"../images/' + result[1])
    }

    console.log('');
    console.log('=============================');
    console.log('==== WFJ | Image mapping ====');
    console.log('=============================');
    console.log('');

    let entitiesRename = {};
    // Replace entities
    entities.forEach(path => {
      let string = path
        .replace(/\//g, '_')
        .replace(/\s+/g, '_')
        .replace('.._images_', '$media_')
        .replace(/\.[^/.]+$/, '"')
        .toLowerCase();
      css = css.replace(path, string);
      entitiesRename[path] = string;
    });

    // To avoid duplicate in the help rename list :)
    Object.keys(entitiesRename).forEach(path => {
      console.log(
        path.replace(/\"/g, ''),
        '====>',
        entitiesRename[path].replace(/\"/g, '').replace('$media_', '')
      );
    });

    const preCss = "<style>" + "\n";
    const postCss = "\n" + "</style>" + "\n";
    data = preCss + css + postCss + data;

    fs.writeFile('livraison/WFJ/index.html', data, 'utf8', function(err) {});

    // JAVASCRIPT in index.html doesnt work on WFJ
    // fs.readFile('public/app.js', 'utf8', function (err, js) {
    //   const preJs = "<script type='text/javascript'>" + "\n" + "PLATFORM = 'WFJ';";
    //   const postJs = "\n" + "</script>" + "\n";
    //   data = preJs + js + postJs + data;
    //   fs.writeFile('livraison/WFJ/index.html', data, 'utf8', function(err) {});
    // });
  });

});