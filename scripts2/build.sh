#!/bin/bash
rm -rf public

# Webpack build
NODE_ENV=production webpack --config scripts/webpack/webpack.production.js

# Copy JS and CSS to livraison/
# cp ./public/app.js ./livraison/ONE/app.js
# cp ./public/app.css ./livraison/ONE/app.css

# Build properties file
node ./scripts/build_i18n_one.js
node ./scripts/build_i18n_fnb.js
node ./scripts/build_i18n_wfj.js

CURRENT_PATH=$PWD

# FNB
if [ -d "${CURRENT_PATH}/livraison/FNB/" ]; then

  # cp ./public/app.js ./livraison/FNB/app.js
  # cp ./public/app.css ./livraison/FNB/app.css

  # Copy Images for FNB
  rsync -r ./images/ ./livraison/FNB/images/

  # Zip project for FNB
  cd ./livraison/FNB/
  rm .DS_Store
  rm ultra-le-teint-a.zip
  zip -r ultra-le-teint-a.zip ./
fi

# WFJ
# if [ -d "${CURRENT_PATH}/livraison/WFJ/" ]; then
  # cp ./public/app.js ./livraison/WFJ/app.js
  # cp ./public/app.css ./livraison/WFJ/app.css
# fi

# Remove webpack default export folder
rm -rf ./public
