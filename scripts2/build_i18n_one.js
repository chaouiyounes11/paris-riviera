const fs = require('fs');
const glob = require('glob');
const _ = require('lodash');
const packageJSON = require('../package.json');
const projectName = packageJSON.name;

const pathToONEPath = (path) => {
  const newString = path
    .replace(/\//g, '_')
    .replace(/\s+/g, '_')
    .replace(/\.[^/.]+$/, '[0].url}')
    .toLowerCase()
    .replace('.._images_', '${freeTextMedia.' + projectName + '_')
    .replace('._images_', '${freeTextMedia.' + projectName + '_');
  return newString;
};

// Build i18n files
glob('**/locale/*.json', {}, function (er, files) {
  files.forEach(function(file) {
    let langContent = require('../' + file);
    try {
      const langContentPlatformSpecific = require('../' + file.replace('locale/', 'locale/ONE/'));
      langContent = _.merge(langContent, langContentPlatformSpecific);
    }
    catch (e) {}
    const langCode = file.replace('locale/', '').replace('.json', '');
    let str = '';
    Object.keys(langContent).map(key => {
      str += key + ' = ' + langContent[key] + ';\n';
    });
    fs.writeFile('livraison/ONE/' + langCode + '.properties', str, err => {});
  });
});

fs.readFile('public/app.js', 'utf8', function (err, js) {
  const preJs = "PLATFORM = 'ONE';";
  js = preJs + js;
  fs.writeFile('livraison/ONE/app.js', js, 'utf8', function(err) {});
});

fs.readFile('moodboard.html', 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }

  // Search for entities in HTML
  const entities = [];
  const reg = /src=\"\.\/images\/([^"]+)/g;
  let result;
  while((result = reg.exec(data)) !== null) {
    entities.push('"../images/' + result[1]);
    const originPath = './images/' + result[1];
    const filePath = pathToONEPath(originPath);
    data = data.replace(originPath, filePath);
  }

  //data = data.replace(/\$\{/g, '$').replace(/\}/g, '');
  fs.readFile('public/app.css', 'utf8', function (err, css) {

    // Search for entities
    //const entities = [];
    const reg = /url\(\"\.\.\/images\/([^)]+)\)/g;
    let result;
    while((result = reg.exec(css)) !== null) {
      entities.push('"../images/' + result[1])
    }

    console.log('');
    console.log('=============================');
    console.log('==== ONE | Image mapping ====');
    console.log('=============================');
    console.log('');

    const packageJSON = require('../package.json');
    const projectName = packageJSON.name;

    let entitiesRename = {};
    // Replace entities
    entities.forEach(path => {
      let string = path
        .replace(/\//g, '_')
        .replace(/\s+/g, '_')
        .replace(/\.[^/.]+$/, '[0].url}"')
        .toLowerCase()
        .replace('.._images_', '${freeTextMedia.' + projectName + '_');
      css = css.replace(path, string);
      entitiesRename[path] = string;
    });

    // To avoid duplicate in the help rename list :)
    Object.keys(entitiesRename).forEach(path => {
      console.log(
        path.replace(/\"/g, ''),
        '====>',
        entitiesRename[path].replace(/\"/g, '').replace('${freeTextMedia.', '').replace('[0].url}', '')
      );
    });

    const beforeAll = '' +
      '<link id="theme" rel="stylesheet" href="https://amp.chanel.com/bundles/app/dist/css/theme.css" />' +
      '<script type="text/javascript" src="https://amp.chanel.com/bundles/app/amp/amp.premier.min.js" charset="utf-8"></script>' +
      '<script type="text/javascript" src="https://amp.chanel.com/bundles/app/dist/js/prod.js" charset="utf-8"></script>' +
      '<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>' + "\n\n";

    const afterAll = '${freeTextjs}';

    const preCss = "<style>" + "\n";
    const postCss = "\n" + "</style>" + "\n";
    data = beforeAll + preCss + css + postCss + data + "\n\n" + afterAll;

    fs.writeFile('livraison/ONE/index.html', data, 'utf8', function(err) {});

    // JAVASCRIPT in index.html doesnt work on ONE
    // fs.readFile('public/app.js', 'utf8', function (err, js) {
    //   const preJs = "<script type='text/javascript'>" + "\n" + "PLATFORM = 'ONE';";
    //   const postJs = "\n" + "</script>" + "\n";
    //   data = preJs + js + postJs + data;
    //   fs.writeFile('livraison/ONE/index.html', data, 'utf8', function(err) {});
    // });
  });

});