const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const loaders = require('./loaders');
const entries = require('./entries');

module.exports = {
  mode: 'development',
  entry: entries,
  output: {
    filename: '[name].js',
    path: __dirname + '/../../public'
  },
  devServer: {
    hot: true,
    port: 3000,
    inline: true,
    disableHostCheck: true,
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      test: /\.scss$/i,
      options: {
        postcss: {
          plugins: [autoprefixer({ browsers: ['last 2 versions'] })]
        }
      }
    }),
    new HtmlWebpackPlugin({
      template: 'index.html'
    })
  ],
  module: {
    rules: loaders,
  }
};