const webpack = require('webpack');
const loaders = require('./loaders-build');
const entries = require('./entries');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: entries,
  output: {
    filename: '[name].js',
    path: __dirname + '/../../public'
  },
  resolve: {
    extensions: ['.js', '.json']
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      test: /\.scss$/i,
      options: {
        postcss: {
          plugins: [autoprefixer({ browsers: ['last 2 versions'] })]
        }
      }
    }),
    new UglifyJSPlugin({
      uglifyOptions: {
        mangle: true,
        ie8: false,
        compress: {
          dead_code: true,
          warnings: false, // Suppress uglification warnings
          pure_getters: true,
          unsafe: true,
          unsafe_comps: true
        },
        output: {
          comments: false
        }
      }
    }),
    new HtmlWebpackPlugin({
      template: './moodboard.html',
      inject: false
    }),
    new ExtractTextPlugin({
      filename: 'app.css'
    })
  ],
  module: {
    rules: loaders
  }
};
