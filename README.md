# Install

> npm install

# Develop

> npm run dev

# Build

> npm run build

# Details

Please don't touch **assets/** and **index.html**.  
You can develop your moodboard in **moodboard.html**, **css/global.scss** & **javascripts/app.js**.  
Your i18n texts should be in **locale/en_US.json** and will be parsed and build in **livraison/en_US.properties**.# chanel_joy
